import logo from './logo.svg';
import './App.css';
import Temperature from './Components/Temperature.js'

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <Temperature/>
      </header>
    </div>
  );
}

export default App;
