import {useEffect, useState} from 'react'
import axios from 'axios'

export default function Temperature() {
    const [temp, setTemp] = useState([])

    useEffect(() => {
        axios.get('http://localhost:8080/api/v1/getTemps').then(res => setTemp(temp => res.data))
    }, [])

    return (
        <div>
            <p>Current temperature:</p>
            {
                temp.map(temp => 
                    <ul key={temp.time_stamp}>
                        <li>Timestamp: {temp.time_stamp}</li>
                        <li>Temperature: {temp.temp}</li>
                    </ul>
                    )
            }
        </div>
    )
}