const express = require('express')
const { Pool } = require('pg')
const pool = new Pool({
    user: 'docker',
    host: 'localhost',
    database: 'tempApp',
    password: 'docker',
    port: '5432',
})

const app = express()
const port = 8080

const body = []

pool.connect((err, client, release) => {
  if (err) {
    return console.error('Error acquiring client', err.stack)
  }
  client.query('SELECT * FROM temps', (err, result) => {
    release()
    if (err) {
      return console.error('Error executing query', err.stack)
    }
    result.rows.map(row => body.push(row))
    console.log(body)
  })
})

app.use((req, res, next) => {
    res.append('Access-Control-Allow-Origin', ['*']);
    res.append('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE');
    res.append('Access-Control-Allow-Headers', 'Content-Type');
    next();
})

app.get('/api/v1/getTemps', (req, res) => {
    res.send(JSON.stringify(body, null, 2))
})

app.listen(port, () => {
    console.log(`Server started at port ${port}`)
})