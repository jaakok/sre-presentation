CREATE DATABASE tempApp;
GRANT ALL PRIVILEGES ON DATABASE tempApp TO docker;
CREATE TABLE temps (
    time_stamp VARCHAR(20),
    temp INT
    );
INSERT INTO temps (time_stamp, temp) VALUES ('2021-10-21', 11);
INSERT INTO temps (time_stamp, temp) VALUES ('2021-10-22', 12);
INSERT INTO temps (time_stamp, temp) VALUES ('2021-10-23', 13);
INSERT INTO temps (time_stamp, temp) VALUES ('2021-10-24', 10);